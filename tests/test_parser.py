"""Module to test parser.py"""

import logging
import os
from pathlib import Path
from unittest.mock import MagicMock

from flywheel_gear_toolkit import GearToolkitContext

from fw_gear_oct_qa.parser import parse_config

log = logging.getLogger(__name__)


def test_parse_config(tmpdir):
    """Here is an example of what you should add in your parse_config Testing"""
    gear_context = MagicMock(spec=GearToolkitContext)
    raw_input = os.path.abspath("tests/assets/oct_volume_test.npy")
    gear_context.get_input_path.return_value = raw_input
    output_dir = Path(tmpdir.mkdir("output"))
    gear_context.output_dir = output_dir
    ophtha_qa, debug = parse_config(gear_context)

    assert gear_context.get_input_path.call_count == 1
    assert ophtha_qa.path_file == raw_input
