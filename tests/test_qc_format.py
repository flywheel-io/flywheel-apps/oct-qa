import logging

import pytest

from fw_gear_oct_qa.format_qc import format_qc_metadata

log = logging.getLogger(__name__)


@pytest.mark.parametrize(
    "qa_index, qc_tests",
    [
        (
            # All tests pass
            {
                "Dynamic range": [1.072, 1.038, 1.021],
                "Outliers Dynamic range": [],
                "Variance extraretinal": [238.947, 273.571, 253.817],
                "Outliers Variance Extraretinal": [],
                "Variance intraretinal": [2129.367, 2161.512, 1861.539],
                "Outliers Variance Intraretinal": [],
                "Sharpness slope": [19.721, 18.585, 18.836],
                "Outliers Sharpness slope": [],
            },
            ["pass", "pass", "pass", "pass", "pass"],
        ),
        (
            # Some tests fail
            {
                "Dynamic range": [1.072, 1.038, 1.021],
                "Outliers Dynamic range": [1],
                "Variance extraretinal": [238.947, 273.571, 253.817],
                "Outliers Variance Extraretinal": [],
                "Variance intraretinal": [2129.367, 2161.512, 1861.539],
                "Outliers Variance Intraretinal": [4, 5],
                "Sharpness slope": [19.721, 18.585, 18.836],
                "Outliers Sharpness slope": [],
            },
            ["fail", "pass", "fail", "pass", "fail"],
        ),
    ],
)
def test_format_qc_metadata(qa_index, qc_tests):
    qc_names_result, qc_test_results, qc_data_results = format_qc_metadata(qa_index)

    # Test qc name order
    qc_names = [
        "dynamic_range",
        "variance_extraretinal",
        "variance_intraretinal",
        "sharpness_slope",
        "overall_qc",
    ]
    for qc_name, qc_name_result in zip(qc_names, qc_names_result):
        assert qc_name == qc_name_result

    # Test results
    for qc_test, qc_test_result, qc_name_result in zip(
        qc_tests, qc_test_results, qc_names_result
    ):
        log.info("qc test: %s" % qc_name_result)
        assert qc_test == qc_test_result
