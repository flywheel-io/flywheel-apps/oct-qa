"""Module to test main.py"""

import os
from pathlib import Path

import numpy as np
import pytest
from flywheel_gear_toolkit import GearToolkitContext

from fw_gear_oct_qa.main import run
from fw_gear_oct_qa.oct_qa import OCTqa


@pytest.mark.parametrize(
    "path_raw_input",
    [
        os.path.abspath("tests/assets/oct_volume_test.npy"),
        os.path.abspath("tests/assets/oct_volume_test.dcm"),
        os.path.abspath("tests/assets/oct_volume_test.dcm.zip"),
    ],
)
def test_run(tmpdir, path_raw_input):
    gear_context = GearToolkitContext(input_args=[])  # 1
    gear_context._out_dir = tmpdir

    work_dir = Path(tmpdir.mkdir("work_dir"))
    output_dir = Path(tmpdir.mkdir("output"))

    ophtha_qa = OCTqa.factory(
        path_file=path_raw_input,
        output_dir=output_dir,
        work_dir=work_dir,
        save_figures_if_fail=True,
    )

    exit_code, QA_index, qc_names, qc_tests, qc_data = run(ophtha_qa=ophtha_qa)
    assert exit_code == 0

    need_to_have = [
        "Dynamic range",
        "Sharpness slope",
        "Variance extraretinal",
        "Variance intraretinal",
    ]
    for key in need_to_have:
        assert key in QA_index

    stats_to_check = {
        "Mean Dynamic range": 1.044,
        "Standard deviation Dynamic range": 0.021,
        "Mean Variance Extraretinal region": 255.445,
        "Standard deviation Variance Extraretinal region": 14.182,
        "Mean Variance Intraretinal": 2050.806,
        "Standard deviation Variance Intraretinal": 134.474,
        "Mean Sharpness slope": 19.047,
        "Standard deviation Sharpness slope": 0.487,
    }

    assert stats_to_check["Mean Dynamic range"] == round(
        np.mean(QA_index["Dynamic range"]), 3
    )
    assert stats_to_check["Standard deviation Dynamic range"] == round(
        np.std(QA_index["Dynamic range"]), 3
    )
    assert stats_to_check["Mean Variance Extraretinal region"] == round(
        np.mean(QA_index["Variance extraretinal"]), 3
    )
    assert stats_to_check["Standard deviation Variance Extraretinal region"] == round(
        np.std(QA_index["Variance extraretinal"]), 3
    )
    assert stats_to_check["Mean Variance Intraretinal"] == round(
        np.mean(QA_index["Variance intraretinal"]), 3
    )
    assert stats_to_check["Standard deviation Variance Intraretinal"] == round(
        np.std(QA_index["Variance intraretinal"]), 3
    )
    assert stats_to_check["Mean Sharpness slope"] == round(
        np.mean(QA_index["Sharpness slope"]), 3
    )
    assert stats_to_check["Standard deviation Sharpness slope"] == round(
        np.std(QA_index["Sharpness slope"]), 3
    )
